#ifndef SERVER_H
#define SERVER_H
#include "Interfaces.h"

class Server : public IX
{
private:
    int a, b, result;
    long m_cRef;
public:
    Server();
    Server(int, int);
    virtual ~Server();
    virtual HRESULT __stdcall QueryInterface(const IID&, void**);
    virtual ULONG __stdcall AddRef();
    virtual ULONG __stdcall Release();

    virtual int __stdcall LCM();
    virtual int __stdcall GCD();
};

class ServerMod : public IX, public IY, public IDispatch
{
private:
    Server* ServerDefautlt;
    int a, b, result;
    long m_cRef;
    int num;
public:
    ServerMod();
    virtual ~ServerMod();
    virtual HRESULT __stdcall QueryInterface(const IID&, void**);
    virtual ULONG __stdcall AddRef();
    virtual ULONG __stdcall Release();

    virtual int __stdcall LCM();
    virtual int __stdcall GCD();
    virtual int __stdcall Sum();

    // IDispatch
    virtual HRESULT __stdcall GetIDsOfNames(REFIID riid, LPOLESTR *rgszNames, UINT cNames, LCID lcid, DISPID *rgDispId);
    virtual HRESULT __stdcall Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS *pDispParams, VARIANT *pVarResult,
                                     EXCEPINFO *pExcepInfo, UINT *puArgErr);

    virtual HRESULT __stdcall GetTypeInfoCount(UINT *pctinfo);
    virtual HRESULT __stdcall GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo);
};

class ServerFactory : public IClassFactory2
{
private:
    long m_cRef;
public:
    ServerFactory();
    virtual ~ServerFactory();
    virtual HRESULT __stdcall CreateInstance(IUnknown*, const IID&, void**);
    virtual HRESULT __stdcall CreateInstance2(IUnknown*, const IID&, void**, int, int);
    virtual HRESULT __stdcall QueryInterface(const IID&, void**);
    virtual ULONG __stdcall AddRef();
    virtual ULONG __stdcall Release();
    virtual HRESULT __stdcall LockServer(BOOL bLock);
};

class ServerModFactory : public IClassFactory
{
private:
    long m_cRef;
public:
    ServerModFactory();
    virtual ~ServerModFactory();
    virtual HRESULT __stdcall CreateInstance(IUnknown*, const IID&, void**);
    virtual HRESULT __stdcall QueryInterface(const IID&, void**);
    virtual ULONG __stdcall AddRef();
    virtual ULONG __stdcall Release();
    virtual HRESULT __stdcall LockServer(BOOL bLock);
};

extern "C" HRESULT __stdcall __declspec(dllexport) DllGetClassObject(const CLSID& clsid, const IID& iid, void** ppv);
#endif